Search Images on Giphy!

  Clone the repository and run

    npm install

Add .env file on the root directory with the Giphy API key
 
     REACT_APP_API_KEY=xxxxxxxxxxx

To start the app locally run:
  
      npm start