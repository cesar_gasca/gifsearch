const axios = require('axios')
const apiKey = process.env.REACT_APP_API_KEY

export default {
  fetchData: async (text) => {
    const url = window.encodeURI(`https://api.giphy.com/v1/gifs/search?api_key=${apiKey}&q=${text}&limit=20&offset=0&rating=G&lang=en`)
    const response = await axios.get(url)
    return response.data.data
  },
  fetchTrending: async () => {
    const url = window.encodeURI(`https://api.giphy.com/v1/gifs/trending?api_key=${apiKey}&limit=20`)
    const response = await axios.get(url)
    return response.data.data
  }
}
