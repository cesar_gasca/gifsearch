import React, { Component } from 'react'
import SearchForm from '../components/SearchForm'
import Results from './Results'
import Loader from '../components/Loader'
import api from '../utils/api'

class Content extends Component {
  constructor () {
    super()
    this.state = {
      data: [],
      searchText: '',
      loading: false
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  loadData (apiAction, searchText) {
    this.setState({
      loading: true
    }, async () => {
      const response = await (searchText ? apiAction(searchText) : apiAction())
      this.setState({
        data: response,
        loading: false
      })
    })
  }

  async componentDidMount () {
    await this.loadData(api.fetchTrending, null)
  }

  handleChange (e) {
    this.setState({
      searchText: e.target.value
    })
  }

  async handleSubmit (e) {
    e.preventDefault()
    await this.loadData(api.fetchData, this.state.searchText)
  }

  render () {
    return (
      <section className='content' >
        <SearchForm
          onChange={this.handleChange}
          onSubmit={this.handleSubmit} />
        {!this.state.loading && <Results data={this.state.data} />}
        {this.state.loading && <Loader />}

      </section>
    )
  }
}

export default Content
