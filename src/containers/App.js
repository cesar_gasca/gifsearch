import React, { Component } from 'react'
import '../styles/App.scss'
import Header from '../components/Header'
import Content from './Content'

class App extends Component {
  render () {
    return (
      <div className='wrapper' >
        <Header />
        <Content />
      </div>
    )
  }
}

export default App
