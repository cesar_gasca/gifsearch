import React from 'react'
import PropTypes from 'prop-types'
import Item from '../components/Item'

const Results = ({ data }) => {
  return (
    <div id='resultContent'>
      {
        data.map(item => {
          return <Item key={item.id} info={item} />
        })
      }
    </div>
  )
}

Results.propTypes = {
  data: PropTypes.array.isRequired
}

export default Results
