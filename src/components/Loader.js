import React from 'react'
import loader from '../images/loader.svg'

const Loader = () => {
  return (
    <div id='loaderContainer'>
      <div className='loaderItem'>
        Loading...
        <img src={loader} alt='Loading' />
      </div>
    </div>
  )
}

export default Loader
