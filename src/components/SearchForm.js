import React from 'react'
import PropTypes from 'prop-types'

const SearchForm = ({ onChange, onSubmit }) => {
  return (
    <div>
      <form className='searchForm' onSubmit={onSubmit}>
        <input
          className='input'
          id='searchInput'
          onChange={onChange}
          placeholder='Type your search...'
        />
        <button id='searchButton'>Find!</button>
      </form>
    </div>
  )
}

SearchForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export default SearchForm
