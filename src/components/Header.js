import React from 'react'

const Header = () => {
  return (
    <header className='header'>Gifsearch</header>
  )
}

export default Header
