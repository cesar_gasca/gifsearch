import React from 'react'

const Item = ({ info }) => {
  return (
    <div className='item'>
      <img src={info.images['fixed_height'].url} alt={info.title} />
    </div>
  )
}

export default Item
